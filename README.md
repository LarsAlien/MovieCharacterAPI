﻿# Movie Character API
## By Fredrik, Ole and Lars

MovieCharacter is an API which exposes endpoints for manipulating data from your local MovieCharacterDb.

## Initial setup
1. Download solution and open in preferred IDE
2. Make sure following nuget packages is installed(Tools->NugetPackageManager->Installed):
	* AutoMapper
	* AutoMapper.Extensions.Microsoft.DependencyInjection
	* Microsoft.EntityFrameworkCore
	* Microsoft.EntityFrameworkCore.Design
	* Microsoft.EntityFrameworkCore.SqlServer
	* Microsoft.EntityFrameworkCore.Tools
	* SwashBuckle.AspNetCore
3. Make sure to edit "DefaultConnection" in appsettings.json to match your local sqlexpress server
4. Open Tools->Nuget package manager->Package manager console
	* RUN: "add-migration InitialDb" (Make the first migrations)
	* RUN: "update-database" (Creates the database on your sqlexpress server)
5. Run "ISS Express" server and Swagger UI with all endpoints opens in your browser.

## Making additional migrations
When you have made changes to domain models you have to:
1. Open Tools->Nuget package manager->Package manager console
2. RUN: "add-migration newMigrationName"
3. RUN: "update-database"

## Endpoints
All endpoints are documented at localhost:port/swagger, powered by Swagger UI
