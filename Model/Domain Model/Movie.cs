﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Model.Domain_Model
{
    public class Movie
    {
        /// <summary> Primary key </summary>
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Title { get; set; }

        /// <summary>
        /// String of comma separated genres
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string Genre { get; set; }

        [Required]
        public int ReleaseYear { get; set; }

        [Required]
        [MaxLength(100)]
        public string Director { get; set; }

        /// <summary> ImageUrl to Movie poster </summary>
        [Required]
        [MaxLength]
        public string ImageUrl { get; set; }

        /// <summary> Url to movie trailer </summary>
        [Required]
        [MaxLength]
        public string TrailerUrl { get; set; }

        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }

        public ICollection<Character> Characters { get; set; }
    }
}
