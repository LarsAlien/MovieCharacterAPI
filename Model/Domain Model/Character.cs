﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Model.Domain_Model
{
    public class Character
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }

        /// <summary>
        /// Optional value for Alias
        /// </summary>
        [MaxLength(100)]
        public string Alias { get; set; }

        [Required]
        [MaxLength(30)]
        public string Gender { get; set; }

        /// <summary>
        /// ImageUrl to Character image
        /// </summary>
        [Required]
        [MaxLength]
        public string ImageUrl { get; set; }

        public ICollection<Movie> Movies { get; set; }

    }
}
