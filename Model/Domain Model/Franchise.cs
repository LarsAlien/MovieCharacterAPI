﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Model.Domain_Model
{
    public class Franchise
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength]
        public string Description { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
