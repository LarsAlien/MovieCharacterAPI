﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Model.Data_Transfer_Objects
{
    public class MovieDTO
    {
        /// <summary> Primary key </summary>
        public int Id { get; set; }

        public string Title { get; set; }

        /// <summary>
        /// String of comma separated genres
        /// </summary>
        public string Genre { get; set; }

        public int ReleaseYear { get; set; }

        public string Director { get; set; }

        /// <summary> ImageUrl to Movie poster </summary>
        public string ImageUrl { get; set; }

        /// <summary> Url to movie trailer </summary>
        public string TrailerUrl { get; set; }

        public int FranchiseId { get; set; }
    }
}
