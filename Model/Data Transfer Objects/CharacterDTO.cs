﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Model.Data_Transfer_Objects
{
    public class CharacterDTO
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int Id { get; set; }

        public string FullName { get; set; }

        /// <summary>
        /// Optional value for Alias
        /// </summary>
        public string Alias { get; set; }

        public string Gender { get; set; }

        /// <summary>
        /// ImageUrl to Character image
        /// </summary>
        public string ImageUrl { get; set; }
    }
}
