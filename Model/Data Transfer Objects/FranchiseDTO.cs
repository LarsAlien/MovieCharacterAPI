﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Model.Data_Transfer_Objects
{
    public class FranchiseDTO
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

    }
}
