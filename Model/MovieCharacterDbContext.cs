﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Model.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Model
{
    public class MovieCharacterDbContext : DbContext
    {
        //Tables

        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movie> Movie { get; set; }


        public MovieCharacterDbContext(DbContextOptions<MovieCharacterDbContext> options) : base(options){}

        //Seeding
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Franchise
            modelBuilder.Entity<Franchise>().HasData(new Franchise()
            {
                Id = 1,
                Name = "Die hard",
                Description = "It is a christmas movie"
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise()
            {
                Id = 2,
                Name = "MCU",
                Description = "Stan Lee is bae"
            });

            //Movie
            modelBuilder.Entity<Movie>().HasData(new Movie()
            {
                Id = 1,
                Title = "Die hard",
                Director = "John McTiernan",
                Genre = "Action,Thriller",
                ImageUrl = "https://www.imdb.com/title/tt0095016/mediaviewer/rm270892032/",
                TrailerUrl = "https://www.imdb.com/video/vi3895704089?playlistId=tt0095016&ref_=tt_ov_vi",
                ReleaseYear  = 1988,
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie()
            {
                Id = 2,
                Title = "Die hard 2",
                Director = "Renny Harlin",
                Genre = "Action,Thriller",
                ImageUrl = "https://www.imdb.com/title/tt0099423/mediaviewer/rm3293919488/",
                TrailerUrl = "https://www.imdb.com/video/vi2417468441?playlistId=tt0099423&ref_=tt_ov_vi",
                ReleaseYear = 1990,
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie()
            {
                Id = 3,
                Title = "Avengers",
                Director = "Joss Whedon",
                Genre = "Action,Adventure,Sci-Fi",
                ImageUrl = "https://www.imdb.com/title/tt0848228/mediaviewer/rm3955117056/",
                TrailerUrl = "https://www.imdb.com/video/vi1891149081?playlistId=tt0848228&ref_=tt_ov_vi",
                ReleaseYear = 2012,
                FranchiseId = 2
            });

            // Characters
            modelBuilder.Entity<Character>().HasData(new Character() {
                Id = 1,
                FullName = "John McClane",
                Gender = "Male",
                ImageUrl = "https://m.media-amazon.com/images/M/" +
                "MV5BMmE2MTI4Y2QtNjA0OC00ZTFiLWFjNjUtNzM0YzgwM2VlNDAzXkEyXkFqcGdeQXVyNzU1NzE3NTg@._V1_CR0,45,480,270_AL_UX477_CR0,0,477,268_AL_.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character()
                {
                    Id = 2,
                    FullName = "Fredrik",
                    Gender = "Attack",
                    ImageUrl = "url.no"
                });

            // Adding MovieCharacter joining table and seeding
            modelBuilder.Entity<Movie>()
                .HasMany(c => c.Characters)
                .WithMany(m => m.Movies)
                .UsingEntity<Dictionary<string, object>>(
                "MovieCharacter",
                r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                rl =>
                {
                    rl.HasKey("CharacterId", "MovieId");
                    rl.HasData(
                        new { CharacterId = 1, MovieId = 1},
                        new { CharacterId = 1, MovieId = 2}
                        );
                }
                );




            base.OnModelCreating(modelBuilder);
        }


    }
}
