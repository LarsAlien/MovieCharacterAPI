﻿using AutoMapper;
using MovieCharacterAPI.Model.Data_Transfer_Objects;
using MovieCharacterAPI.Model.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    /// <summary>
    /// AutoMapper profile
    /// </summary>
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>();
        }
    }
}
