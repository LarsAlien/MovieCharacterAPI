﻿using AutoMapper;
using MovieCharacterAPI.Model.Data_Transfer_Objects;
using MovieCharacterAPI.Model.Domain_Model;

namespace MovieCharacterAPI.Profiles
{
    /// <summary>
    /// AutoMapper profile
    /// </summary>
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>();
        }
    }

}
