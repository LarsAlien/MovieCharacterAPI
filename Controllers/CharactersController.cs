﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Model;
using MovieCharacterAPI.Model.Data_Transfer_Objects;
using MovieCharacterAPI.Model.Domain_Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all characters
        /// </summary>
        // GET: api/v1/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharacter()
        {
            List<CharacterDTO> characterDTOs = _mapper.Map<List<CharacterDTO>>(await _context.Character.ToListAsync());
            return characterDTOs;
        }

        /// <summary>
        /// Returns character by id
        /// </summary>
        // GET: api/v1/Characters/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacter(int id)
        {
            CharacterDTO character = _mapper.Map<CharacterDTO>(await _context.Character.FindAsync(id));

            if (character == null)
            {
                return NotFound();
            }

            return character;
        }

        /// <summary>
        /// Updates character with given id
        /// </summary>
        // PUT: api/v1/Characters/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterDTO characterDTO)
        {

            if (id != characterDTO.Id)
            {
                return BadRequest();
            }
            Character character = new Character()
            {
                Id = id,
                FullName = characterDTO.FullName,
                Alias = characterDTO.Alias,
                ImageUrl = characterDTO.ImageUrl,
                Gender = characterDTO.Gender
            };

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds new character
        /// </summary>
        // POST: api/v1/Characters
        [HttpPost]
        public async Task<ActionResult<CharacterDTO>> PostCharacter(CreateCharacterDTO characterDTO)
        {
            List<Movie> movies = new List<Movie>();
            foreach (string id in characterDTO.movieIds.Split(","))
            {
                movies.Add(_context.Movie.Find(Int32.Parse(id)));
            }
            Character character = new Character()
            {
                FullName = characterDTO.FullName,
                Alias = characterDTO.Alias,
                ImageUrl = characterDTO.ImageUrl,
                Gender = characterDTO.Gender,
                Movies = movies
            };
            _context.Character.Add(character);
            await _context.SaveChangesAsync();

            characterDTO.Id = character.Id;

            return CreatedAtAction("GetCharacter", new { id = character.Id }, characterDTO);
        }

        /// <summary>
        /// Deletes character with given id
        /// </summary>
        // DELETE: api/v1/Characters/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Character.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Get specific characters movies
        /// </summary>
        // GET: api/v1/Characters/{id}/Movies
        [HttpGet("{id}/Movies")]
        public ActionResult<IEnumerable<MovieDTO>> GetCharacterMovies(int id)
        {
            Character character = _context.Character.Include(c => c.Movies).Where(c => c.Id == id).Single();
            List<MovieDTO> movieDTOs = _mapper.Map<List<MovieDTO>>(character.Movies);
            return movieDTOs;
        }

        /// <summary>
        /// Adds new movie to specific character
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <param name="movieId">Movie Id of which movie to add</param>
        // POST: api/v1/Characters/{id}/Movies
        [HttpPost("{id}/Movies/{movieId}")]
        public async Task<IActionResult> PostCharacterMovie(int id, int movieId)
        {
            Character character = await _context.Character.Include(c => c.Movies).SingleAsync(c => c.Id == id);
            Movie movie = await _context.Movie.FindAsync(movieId);
            if (!character.Movies.Contains(movie)) character.Movies.Add(movie);

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Removes a movie from specific character
        /// </summary>
        /// <param name="id">Character Id</param>
        /// <param name="movieId">Movie Id of which movie to remove</param>
        // DELETE: api/v1/Characters/{id}/Movies/{movieId}
        [HttpDelete("{id}/Movies/{movieId}")]
        public async Task<IActionResult> RemoveCharacterMovie(int id, int movieId)
        {
            Character character = _context.Character.Include(c => c.Movies).Where(c => c.Id == id).Single();
            if (character == null)
            {
                return NotFound();
            }

            character.Movies.Remove(character.Movies.Single(m => m.Id == movieId));

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Character.Any(e => e.Id == id);
        }
    }
}
