﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Model;
using MovieCharacterAPI.Model.Data_Transfer_Objects;
using MovieCharacterAPI.Model.Domain_Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public FranchisesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all franchises
        /// </summary>
        // GET: api/v1/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchise()
        {
            return _mapper.Map<List<FranchiseDTO>>(await _context.Franchise.ToListAsync());
        }

        /// <summary>
        /// Returns franchise by id
        /// </summary>
        // GET: api/v1/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchise(int id)
        {
            FranchiseDTO franchiseDTO = _mapper.Map<FranchiseDTO>(await _context.Franchise.FindAsync(id));

            if (franchiseDTO == null)
            {
                return NotFound();
            }

            return franchiseDTO;
        }

        /// <summary>
        /// Updates franchise with given id
        /// </summary>
        // PUT: api/v1/Franchises/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseDTO franchiseDTO)
        {
            if (id != franchiseDTO.Id)
            {
                return BadRequest();
            }

            Franchise franchise = new Franchise()
            {
                Id = id,
                Name = franchiseDTO.Name,
                Description = franchiseDTO.Description
            };

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds new franchise
        /// </summary>
        // POST: api/v1/Franchises
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseDTO franchiseDTO)
        {
            Franchise franchise = new Franchise()
            {
                Name = franchiseDTO.Name,
                Description = franchiseDTO.Description
            };
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();
            franchiseDTO.Id = franchise.Id;
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        /// <summary>
        /// Deletes franchise by id
        /// </summary>
        // DELETE: api/v1/Franchises/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Returns all movies in given franchise
        /// </summary>
        // GET: api/v1/franchises/{id}/movies
        [HttpGet("{id}/Movies")]
        public ActionResult<IEnumerable<MovieDTO>> GetFranchiseMovies(int id)
        {
            Franchise franchise = _context.Franchise.Include(f => f.Movies).Where(f => f.Id == id).Single();
            if (franchise == null)
            {
                return NotFound();
            }
            List<MovieDTO> movieDTOs = _mapper.Map<List<MovieDTO>>(franchise.Movies);
            return movieDTOs;
        }

        /// <summary>
        /// Returns all characters in given franchise
        /// </summary>
        // GET: api/v1/franchises/{id}/characters
        [HttpGet("{id}/Characters")]
        public ActionResult<IEnumerable<CharacterDTO>> GetFranchiseCharacters(int id)
        {
            Franchise franchise = _context.Franchise.Include(f => f.Movies).ThenInclude(m => m.Characters).Where(f => f.Id == id).Single();
            if (franchise == null)
            {
                return NotFound();
            }
            HashSet<CharacterDTO> characterDTOs = franchise.Movies.SelectMany(m => m.Characters.Select(c => _mapper.Map<CharacterDTO>(c))).ToHashSet();
            return characterDTOs.ToList();
        }


        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.Id == id);
        }
    }
}
