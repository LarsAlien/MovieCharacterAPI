﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Model;
using MovieCharacterAPI.Model.Data_Transfer_Objects;
using MovieCharacterAPI.Model.Domain_Model;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Returns all movies
        /// </summary>
        // GET: api/v1/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieDTO>>(await _context.Movie.ToListAsync());
        }

        /// <summary>
        /// Returns specific movie by id
        /// </summary>
        // GET: api/Movies/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovie(int id)
        {
            MovieDTO movieDTO = _mapper.Map<MovieDTO>(await _context.Movie.FindAsync(id));

            if (movieDTO == null)
            {
                return NotFound();
            }

            return movieDTO;
        }

        /// <summary>
        /// Updates movie with given id
        /// </summary>
        // PUT: api/v1/Movies/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieDTO movieDTO)
        {
            if (id != movieDTO.Id)
            {
                return BadRequest();
            }
            Movie movie = new Movie()
            {
                Id = id,
                Title = movieDTO.Title,
                Genre = movieDTO.Genre,
                Director = movieDTO.Director,
                ImageUrl = movieDTO.ImageUrl,
                TrailerUrl = movieDTO.TrailerUrl,
                Franchise = _context.Franchise.Find(movieDTO.FranchiseId)
            };

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id)) return NotFound();

                else throw;
            }

            return NoContent();
        }

        /// <summary>
        /// Adds new movie
        /// </summary>
        // POST: api/v1/Movies
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieDTO movieDTO)
        {
            Movie movie = new Movie()
            {
                Title = movieDTO.Title,
                Genre = movieDTO.Genre,
                Director = movieDTO.Director,
                ImageUrl = movieDTO.ImageUrl,
                TrailerUrl = movieDTO.TrailerUrl,
                Franchise = _context.Franchise.Find(movieDTO.FranchiseId)};
            _context.Movie.Add(movie);
            await _context.SaveChangesAsync();

            movieDTO.Id = movie.Id;

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movieDTO);
        }

        /// <summary>
        /// Deletes movie by id
        /// </summary>
        // DELETE: api/v1/Movies/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Returns all characters in specific movie
        /// </summary>
        // GET: api/v1/Movies/{id}/Characters
        [HttpGet("{id}/Characters")]
        public ActionResult<IEnumerable<CharacterDTO>> GetMovieCharacters(int id)
        {
            Movie movie = _context.Movie.Include(m => m.Characters).Where(m => m.Id == id).Single();
            return _mapper.Map<List<CharacterDTO>>(movie.Characters);
        }

        /// <summary>
        /// Returns specific movies franchise
        /// </summary>
        // GET: api/v1/Movies/{id}/Franchise
        [HttpGet("{id}/Franchise")]
        public ActionResult<FranchiseDTO> GetMovieFranchise(int id)
        {
            Movie movie = _context.Movie.Include(m => m.Franchise).Single(m => m.Id == id);
            if (movie == null)
            {
                return NotFound();
            }
            return _mapper.Map<FranchiseDTO>(movie.Franchise);
        }


        /// <summary>
        /// Adds/replaces franchise on given movie
        /// </summary>
        /// <param name="id">Movie Id</param>
        /// <param name="franchiseId">Franchise Id of which franchise to add</param>
        // POST: api/v1/Movies/{id}/Franchise/{franchiseId}
        [HttpPost("{id}/Franchise/{franchiseId}")]
        public async Task<IActionResult> PostMovieFranchise(int id, int franchiseId)
        {
            Movie movie = await _context.Movie.Include(m => m.Franchise).SingleAsync(m => m.Id == id);
            movie.Franchise = await _context.Franchise.FindAsync(franchiseId);

            if (movie.Franchise == null)
            {
                return NotFound();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id)) return NotFound();

                else throw;
            }

            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.Id == id);
        }
    }
}
